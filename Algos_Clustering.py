
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm as cm
import matplotlib.cm as cm

import scipy
import scipy.cluster.hierarchy as sch
import time

from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn import cluster, datasets
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_samples, silhouette_score, calinski_harabaz_score
from IPython.core.display import display, HTML
import skfuzzy as fuzz

from pylab import *

def getKey(item):
	return item[0]

class AlgoCLuster(object):
	def __init__(self, Name, ncluster, metric, clusLib = 'sklearn'):
		self.Name = Name
		self.ncluster = ncluster
		self.metric = metric
		self.algoritm = None
		self.D = []
		self.df=None
		self.y_pred=None
		self.corr=None
		self.Calinski_Silhouette=[]
		self.clusLib=clusLib
        
	def plot_all(self):
		self.plot_matrix()
		self.plot_siluet()
		self.plot_scores_Calinski_Silhouette()

	def getNclusters(self):
		return self.ncluster

	def setNclusters(self,ncluster):
		self.ncluster = ncluster

	def getMetric(self):
		return self.metric

	def setMetric(self,metric):
		self.metric = metric

	def fitt(self, D, C):
		if(self.clusLib == 'sklearn'):
			self.algoritm.n_clusters = C
			self.algoritm.fit(D)
			return self.algoritm.labels_.astype(np.int), None
		else:
			cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(D, C, 2, error=0.0005, maxiter=4000, init=None)
			return np.argmax(u, axis=0), fpc
			

	#Entrena y predice
	def FitAndPredict(self):
		self.y_pred = self.fitt(self.D, self.ncluster)[0]

	#crea las agrupaciones en un diccionario 
	def PredictGroupClusters(self):
		self.FitAndPredict()

		Grupos={}
		for i in range(self.ncluster):
			Grupos['Cluster_'+str(i).zfill(3)]=[]
		names=self.df.columns.tolist()
		for j,i in enumerate(self.y_pred):
			Grupos['Cluster_'+str(i).zfill(3)].append(names[j])
		return Grupos



	def plot_siluet(self,var=True,title=''):
		D,y_pred=self.D,self.y_pred
		if(var):
			plt.figure()
		##### Define si la metrica es precomputada
		if(self.metric=='precomputed'):
			silhouette_avg = silhouette_score(X=D,metric='precomputed',labels=y_pred)
			sample_silhouette_values = silhouette_samples(X=D,metric='precomputed',labels=y_pred)
		else:
			silhouette_avg = silhouette_score(X=D,labels=y_pred)
			sample_silhouette_values = silhouette_samples(X=D,labels=y_pred)
		    

		calinski_score = calinski_harabaz_score(X=D, labels=y_pred)

		plt.xlim([-1, 1])
		plt.ylim([0, y_pred.shape[0] + (self.ncluster + 1) * 10])
		y_lower = 10

		for i in range(self.ncluster):
			# Aggregate the silhouette scores for samples belonging to
			# cluster i, and sort them
			ith_cluster_silhouette_values = sample_silhouette_values[y_pred == i]
			ith_cluster_silhouette_values.sort()
			size_cluster_i = ith_cluster_silhouette_values.shape[0]
			y_upper = y_lower + size_cluster_i

			color = cm.Spectral(float(i) / self.ncluster)
			plt.fill_betweenx(np.arange(y_lower, y_upper),0, ith_cluster_silhouette_values, facecolor=color, edgecolor=color, alpha=0.7)
			# Label the silhouette plots with their cluster numbers at the middle
			plt.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

			# Compute the new y_lower for next plot
			y_lower = y_upper + 10  # 10 for the 0 samples
		# The vertical line for average silhouette score of all the values
		plt.axvline(x=silhouette_avg, color="red", linestyle="--")
		plt.yticks([])  # Clear the yaxis labels / ticks
		plt.grid(True)
		plt.xticks([-0.4,-0.2, 0, 0.2, 0.4, 0.6, 0.8, 1],rotation=45)
		plt.xlim([-0.4, 0.8])
		plt.title(title)
	
		if(var):	
			plt.title('Silhouette Coefficient for each sample')
			plt.show()
		return None

	def getMatrixCorr(self):
		idx1=[i[1] for i in sorted([[j,i] for i,j in enumerate(self.y_pred)], key=getKey)]
		Dtem=self.corr.as_matrix()
		Dtem = Dtem[idx1,:]
		Dtem = Dtem[:,idx1]
		return Dtem

	def plot_matrix(self):

		idx1=[i[1] for i in sorted([[j,i] for i,j in enumerate(self.y_pred)], key=getKey)]
		Dtem=self.corr.as_matrix()
		Dtem = Dtem[idx1,:]
		Dtem = Dtem[:,idx1]


		fig = plt.figure()
		ax1 = fig.add_subplot(111)
		cmap = cm.get_cmap('jet', 30)
		cax = ax1.matshow(Dtem, interpolation="nearest", cmap=cmap, vmin=0, vmax=1)

		plt.grid(False)
		plt.title(self.Name)

		plt.xticks([])
		plt.yticks([])
		#plt.xticks(range(len(corr.columns)),rot5tation = 90)
		#plt.yticks(range(len(corr.columns)))
		#plt.xticks(range(len(corr.columns)), corr.columns,rotation = 45);
		#plt.yticks(range(len(corr.columns)), corr.columns);
		#plt.xticks(range(len(corr[:,0])),idx1,rotation = 90);
		#plt.yticks(range(len(corr[:,0])),idx1);
		#Add colorbar, make sure to specify tick locations to match desired ticklabels

		cbar = fig.colorbar(cax, ticks=[-1,0,1])

		plt.show()


	def plot_scores_Calinski_Silhouette(self):
		n_clusters=[i for i in range(2,60,2)]
		D=self.D

		paramet={}
		paramet['Silhouette Coefficient']=[]
		paramet['Calinski and Harabaz score']=[]

		if(self.clusLib != 'sklearn'):
			paramet['FPC']=[]
			
		for c in n_clusters:
			y_pred, fpc = self.fitt(D, c)
			calinski_score = calinski_harabaz_score(X=D, labels=y_pred)
			if(self.metric=='precomputed'):
				silhouette_avg = silhouette_score(X=D,metric='precomputed',labels=y_pred)
			else:
				silhouette_avg = silhouette_score(X=D,labels=y_pred)
		
			paramet['Silhouette Coefficient'].append(silhouette_avg)
			paramet['Calinski and Harabaz score'].append(calinski_score)
			if(self.clusLib != 'sklearn'):
				paramet['FPC'].append(fpc)

		for i,cu in enumerate(paramet):
			x=n_clusters
			y=paramet[cu]
			self.Calinski_Silhouette.append([x,y,cu+'\n score',self.ncluster])
			
			if(self.clusLib != 'sklearn'):
				plt.figure(figsize=(5,6))
				plt.subplot(311+i)
			else:
				plt.figure(figsize=(5,4))
				plt.subplot(211+i)
			plt.title(cu)
			plt.grid(True)
			plt.xlabel('#Clusters')
			plt.ylabel('Score')
			plt.plot(x,y)
			plt.axvline(x=self.ncluster, color="red", linestyle="--")

		plt.tight_layout()
		plt.show()


	def DataManagerType(f):
		def nf(*args,**kwargs):
			if(len(args)==5):
				f(*args,**kwargs)
			elif(len(args)==2):
				args[0].corr=args[1].D0
				args[0].df=args[1].get_df() 
				f(args[0],args[1].D0,args[1].D1,args[1].D2)
		return nf
	DataManagerType = staticmethod( DataManagerType )



#KMeans
class CKMeans(AlgoCLuster):
	def __init__(self):
		AlgoCLuster.__init__(self,'KMeans',10,'precomputed')


	@AlgoCLuster.DataManagerType
	def ConfigClus(self,D0,D1,D2):
		self.D=D2
		self.algoritm = cluster.KMeans(random_state=0,precompute_distances=True)
    
        

#AgglomerativeClustering linkage=complete
class CAgglomerativeClustering_1(AlgoCLuster):
	def __init__(self):
		AlgoCLuster.__init__(self,'CL',10,'precomputed')

	@AlgoCLuster.DataManagerType
	def ConfigClus(self,D0,D1,D2):
		self.D=D2
		self.algoritm = cluster.AgglomerativeClustering(affinity=self.metric, linkage='complete')
        

#AgglomerativeClustering linkage=average
class CAgglomerativeClustering_2(AlgoCLuster):
	def __init__(self):
		AlgoCLuster.__init__(self,'AL',6,'precomputed')

	@AlgoCLuster.DataManagerType
	def ConfigClus(self,D0,D1,D2):
		self.D=D2
		self.algoritm = cluster.AgglomerativeClustering(linkage="average", affinity=self.metric)
        

#SpectralClustering
class CSpectralClustering(AlgoCLuster):
	def __init__(self):
		AlgoCLuster.__init__(self,'Spectral',10,None)

	@AlgoCLuster.DataManagerType
	def ConfigClus(self,D0,D1,D2):
		self.D=np.exp(- D1 ** 2 / (2. * 0.7 ** 2))
		self.algoritm = cluster.SpectralClustering(eigen_solver=None)
        

#MiniBatchKMeans
class CMiniBatchKMeans(AlgoCLuster):
	def __init__(self):
		AlgoCLuster.__init__(self,'MBKMeans',12,None)

	@AlgoCLuster.DataManagerType
	def ConfigClus(self,D0,D1,D2):
		self.D=D2
		self.algoritm = cluster.MiniBatchKMeans()

#Birch
class CBirch(AlgoCLuster):
	def __init__(self):
		AlgoCLuster.__init__(self,'Birch',10,None)

	@AlgoCLuster.DataManagerType
	def ConfigClus(self,D0,D1,D2):
		self.D=D2
		self.algoritm = cluster.Birch()
        

#AgglomerativeClustering ward
class CAgglomerativeClustering_3(AlgoCLuster):
	def __init__(self):
		AlgoCLuster.__init__(self,'WM',10,'precomputed')

	@AlgoCLuster.DataManagerType
	def ConfigClus(self,D0,D1,D2):
		self.D=D2
		connectivity = kneighbors_graph(self.D, n_neighbors=10, include_self=False)
		connectivity = 0.5 * (connectivity + connectivity.T)
		self.algoritm = cluster.AgglomerativeClustering(linkage='ward',connectivity=connectivity)
        

#fuzzy 1
class CFuzzy(AlgoCLuster):
	def __init__(self):
		AlgoCLuster.__init__(self,'Fuzzy',8,None,'fuzzy')

	@AlgoCLuster.DataManagerType
	def ConfigClus(self,D0,D1,D2):
		self.D=D2
		self.algoritm = None



class ClusterManager:
	def __init__(self):

		self.__FunctionsName={
			'CKMeans':{'nemo':'KMeans'},
			'CAgglomerativeClustering_1':{'nemo':'CL'},
			'CAgglomerativeClustering_2':{'nemo':'AL'},
			'CSpectralClustering':{'nemo':'Spectral'},
			'CMiniBatchKMeans':{'nemo':'MBKMeans'},
			'CBirch':{'nemo':'Birch'},
			'CAgglomerativeClustering_3':{'nemo':'WM'},
			'CFuzzy':{'nemo':'CFuzzy'}
		}


		self.__ClustersEvaluados=self.__FunctionsName.copy()
		self.__DicTemPlotUnity=self.__FunctionsName.copy()
		self.__Names=[i for i in self.__FunctionsName]
    
	def getFuncName(self):
		return self.__FunctionsName.copy()

	def getName(self):
		return self.__Names.copy()

	def InstanciaClusters(self):
		for i,key in enumerate(self.__FunctionsName):
			algo=globals()[key]()
			self.__FunctionsName[key]['Name']=algo.Name
			self.__ClustersEvaluados[key]['Name']=algo.Name


	def EvaluaClusters(self,Datos):
		for i,key in enumerate(self.__ClustersEvaluados):
			algo=globals()[key]()
			self.__ClustersEvaluados[key]['Name']=algo.Name
			algo.ConfigClus(Datos)
			Grupos=algo.PredictGroupClusters()
			self.__ClustersEvaluados[key]['Groups']=Grupos


	def getClusterEvaluations(self):
		return self.__ClustersEvaluados 

	def TestTrainAndPlot(self,Datos):
		for i,key in enumerate(self.__FunctionsName):
			print(i)
			algo=globals()[key]()
			display(HTML('<center><h1>'+algo.Name+'</h1></center>'))
			algo.ConfigClus(Datos)
			algo.FitAndPredict()
			algo.plot_all()
			self.__DicTemPlotUnity[key]['Name']=algo.Name
			self.__DicTemPlotUnity[key]['Matrix']=algo.getMatrixCorr()
			self.__DicTemPlotUnity[key]['Calinski_Silhouette']=algo.Calinski_Silhouette

	def MatrixArrayPlot(self,D0,title): 
		plt.figure(figsize=(15,4))

		plt.subplot(2,5,1)
		    
		plt.imshow(D0, interpolation="nearest", cmap=cm.get_cmap('jet', 30), vmin=0, vmax=1)
		plt.grid(False)
		plt.title(title)
		plt.colorbar()
		plt.xticks([])
		plt.yticks([])

		dl=2
		for i,key in enumerate(self.__DicTemPlotUnity):
			if(i==3):
				dl=3

			plt.subplot(2,5,i+dl)


			plt.imshow(self.__DicTemPlotUnity[key]['Matrix'], interpolation="nearest", cmap=cm.get_cmap('jet', 10), vmin=0, vmax=1)
			plt.grid(False)
			plt.title(self.__DicTemPlotUnity[key]['nemo'])
			plt.colorbar()
			plt.xticks([])
			plt.yticks([])

		#cax = ax1.matshow(D0, interpolation="nearest", cmap=cm.get_cmap('jet', 30), vmin=0, vmax=1)
		#plt.subplot(1,5,4)    
		#ax = plt.gca()
		#ax.set_aspect('equal')
		#ax.set_frame_on(False)
		#ax.get_xaxis().tick_bottom()
		#ax.axes.get_yaxis().set_visible(False)
		#ax.axes.get_xaxis().set_visible(False)  
		#plt.colorbar(ticks=[-1,0,1])

		plt.show()


	def MatrixArrayPlot2(self,D0,title): 

		fig, axes = plt.subplots(nrows=2, ncols=4)
		lista=[D0]
		for key in self.__DicTemPlotUnity:
			lista.append(self.__DicTemPlotUnity[key]['Matrix'])

		for (i,item),ax in zip(enumerate(lista),axes.flat):
			im = ax.imshow(item, vmin=0, vmax=1)
			ax.title(self.__DicTemPlotUnity[key]['nemo'])

		#cax = fig.add_axes([0.9, 0.1, 0.03, 0.8])
		cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
		fig.colorbar(im, cax=cbar_ax)
		plt.tight_layout()
		plt.show()


	def Calinski_SilhouetteArrayPlot(self):
		for q in [0,1]: 
			plt.figure(figsize=(17,4))
			for i,key in enumerate(self.__DicTemPlotUnity):
				p = self.__DicTemPlotUnity[key]['Calinski_Silhouette'][q]
				plt.subplot(2,8,i+1+8*q)
				x=p[0]
				y=p[1]
				cu=p[2]

				plt.title(self.__DicTemPlotUnity[key]['nemo'])
				plt.xlabel('#Clusters')
				plt.grid(True)

				if(i==0):
					plt.ylabel(cu)
				plt.plot(x,y)
				plt.axvline(x=p[3], color="red", linestyle="--")
				plt.tight_layout()
		plt.show()

	def SilhouettePlotArray(self,Datos):
		plt.figure(figsize=(14,4))
		for i,key in enumerate(self.__FunctionsName):
			plt.subplot(1,8,i+1)
			algo=globals()[key]()
			algo.ConfigClus(Datos)
			algo.FitAndPredict()
			algo.plot_siluet(False,self.__DicTemPlotUnity[key]['nemo'])
			plt.tight_layout()
		plt.show()
            

